#!/usr/bin/python3


# https://github.com/Cangjians/libcangjie/issues/111

import os
import string
import time

import bs4
import requests

from _utils import get_lines


TABLE_FILE = 'data/table.txt'
TMP_FILE = 'data/table.txt.tmp'
VALID_TEXT = string.ascii_lowercase + ' '


def get_cj5_order(char, current_cj5):
    # Let's be nice to the ChineseCJ folks and not hammer their server down.
    # They provide a valuable service, so we can wait a bit instead.
    time.sleep(0.2)

    url = f'http://www.chinesecj.com/cj5dict/index.php?stype=1&cj={char}'

    response = requests.get(url)

    if response.status_code != 200:
        raise NotImplementedError(f'Got response HTTP {response.status_code} for {char}')

    soup = bs4.BeautifulSoup(response.text, features='lxml')

    # Warning: extremely fragile web scraping!
    # Fortunately they do not change their site often, if ever.

    trs = soup.find_all('tr', valign='center', bgcolor='dbedfe')

    if not trs:
        # They do not know this character :(
        return current_cj5

    if len(trs) > 1:
        raise NotImplementedError(f'Could not single out the table row for {char}')

    tr = trs[0]
    td = tr.find_all('td')[3]
    text = ''.join([c for c in td.text if c in VALID_TEXT])
    cj5 = [code for code in text.split(' ') if code]

    for c in current_cj5:
        if c not in cj5:
            # Add it back, at the end, to avoid losing compatibility things we had
            cj5.append(c)

    return cj5


with open(TMP_FILE, 'w') as f:
    for line in get_lines(TABLE_FILE):
        if isinstance(line, str):
            # This is a comment, or an empty line
            f.write(str(line))

        elif line.zh == '0' or len(line.cj5) <= 1:
            f.write(str(line))

        else:
            # This is a Chinese character with multiple Cangjie 5 codes, let's
            # reorder them.
            line.cj5 = get_cj5_order(line.char, line.cj5)
            f.write(str(line))

os.rename(TMP_FILE, TABLE_FILE)
