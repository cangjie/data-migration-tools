class TableLine:
    @classmethod
    def from_text_line(cls, line):
        l = cls()
        (l.char, l.sc, l.zh, l.b5, l.hk, l.tw, l.kj, l.hira, l.kata, l.punc,
            l.symb, l.cj3, l.cj5, l.short, l.freq) = line.split(' ')

        l.cj3 = l.cj3.split(',') if l.cj3 != 'NA' else []
        l.cj5 = l.cj5.split(',') if l.cj5 != 'NA' else []
        l.short = l.short if l.short != 'NA' else None

        return l

    def __str__(self):
        cj3 = ','.join(self.cj3) if self.cj3 else 'NA'
        cj5 = ','.join(self.cj5) if self.cj5 else 'NA'
        short = self.short if self.short else 'NA'

        return ' '.join((
            self.char, self.sc, self.zh, self.b5, self.hk, self.tw, self.kj,
            self.hira, self.kata, self.punc, self.symb, cj3, cj5, short,
            self.freq,
        )) + '\n'


def get_lines(tablepath):
    with open(tablepath, 'r') as table:
        for line in table:
            line = line.strip(' \n')

            if not line or line.startswith('#'):
                yield line + '\n'
                continue

            yield TableLine.from_text_line(line)
